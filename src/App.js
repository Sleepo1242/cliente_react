import React from 'react';
//import logo from './logo.svg';
import './App.css';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ListarClienteComponent from './components/ListarClientesComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import CrearClienteComponent from './components/CrearClienteComponent';
import VerClienteComponent from './components/VerClienteComponent';



function App() {
  return (
    <div>
      <Router>
        <HeaderComponent>
          <div>
            <Switch>
              <Route path="/" exact component = {ListarClienteComponent}></Route>
              <Route path="/clientes" component = {ListarClienteComponent}></Route>
              <Route path="/add-cliente/:id" component = {CrearClienteComponent}></Route>
              <Route path="/view-cliente/:id" component = {VerClienteComponent}></Route>
            </Switch>
          </div>
        </HeaderComponent>
      </Router>
    </div>
  );
}

export default App;
