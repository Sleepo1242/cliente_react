import axios from 'axios';

const CLIENT_API_BASE_URL = "http://localhost:8091/cliente";

class ClienteService {
    getCliente() {
        return axios.get(CLIENT_API_BASE_URL);
    }

    crearCliente(cliente) {
        return axios.post(CLIENT_API_BASE_URL, cliente);
    }

    getClienteById(clienteId) {
        return axios.post(CLIENT_API_BASE_URL + '/' + clienteId);
    }

    actualizarCliente(cliente, clienteId) {
        return axios.post(CLIENT_API_BASE_URL, cliente);
    }

    eliminarCliente(clienteId) {
        return axios.post(CLIENT_API_BASE_URL + '/' + clienteId);
    }
}

export default new ClienteService()